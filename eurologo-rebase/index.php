<?php
include './templates/header.php';
?>
<?php
if (ereg("MSIE", $_SERVER["HTTP_USER_AGENT"])) {
  header('Location: ./IE/index.php');
  exit();
}
?>

<br>
<center>
	<div class="my-slider" class="responsive-img">
		<ul>
			<li>
				<img src="./photos/index/DSC01955.JPG">
				<center>
					<h3> Sérigraphie à plat </h3>
				</center>
			</li>
			<li>
				<img src="./photos/index/DSC01942.JPG">
				<center>
					<h3> Sérigraphie textile </h3>
				</center>
			</li>
			<li>
				<img src="./photos/index/20150901_154644.jpg">
				<center>
					<h3>  Transfert-Sublimation </h3>
				</center>
			</li>
			<li>
				<img src="./photos/index/DSC01911.JPG">
				<center>
					<h3>  Broderie </h3>
				</center>
			</li>
			<li>
				<img src="./photos/index/DSC01928.JPG">
				<center>
					<h3>  Numérique </h3>
				</center>
			</li>
			<li>
				<img src="./photos/index/20150901_154644.jpg">
				<center>
					<h3>Lettrage adhésif </h3>
				</center>
			</li>
		</ul>
	</div>
</center>
<div class="reduct-text">
	<h3> Entreprise artisanale de Sérigraphie fondée en 1989 par Moïse Létienne, effectif actuel: 6 personnes. </h3>
	<h4>Nos activités sont:</h4>
	<li>Sérigraphie sur plat: Panneaux, autocollants, stickers, ...</li>
	<li> Sérigraphie sur textile: T.shirt , casquettes,coupe vent,sweat,vetements de travail,... </li>
	<li> Lettrage adhésif: Décoration de véhicules, enseignes, panneaux, calicots,...</li>
	<li> Impression numérique: Stickers, baches publicitaires, affiche ... </li>
	<li> Broderie sur textile : vêtements ,casquettes,... </li>
	<li> Sublimation sur textile et objets </li>
	<h3>Des moyens et des hommes </h3>
	<p>
		 Notre entreprise artisanale de taille humaine composé d'un parc machines varié et complémentaire et d'un personnel fort de 20 ans d'expérience dans le domaine, répondra à l'ensemble de vos demandes publicitaires.
	</p>
	<p>
		Téléphone : 03.20.64.80.87
	</p>
	<p>
		eurologo@wanadoo.fr 
	</p>
	<a href="mailto:eurologo@wanadoo.fr?Subject=Contact" target="_top" class="waves-effect waves-light btn-large"> Nous envoyer un mail :)</a>
</div>
<div class="iframe">
	<br>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.2168229668478!2d3.202252915370969!3d50.45568699486914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2c5396b68688d%3A0xb56e617614119136!2s900+Route+nationale%2C+59310+Coutiches!5e0!3m2!1sfr!2sfr!4v1478527827225" width="425" height="300" frameborder="2" allowfullscreen class="responsive-img"></iframe>
</div>
<div class="iframe">
<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sfr!2sfr!4v1484049754145!6m8!1m7!1s4iciG_djmDBXE1r1zk42LQ!2m2!1d50.4558045702731!2d3.204107109714585!3f112.99331367714949!4f-5.096615652702809!5f0.7820865974627469" width="425" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>


<div class="container">
<?php
include './js/scriptjs.js';
include './js/sidenav.js';
include './templates/footer.php';
?>

