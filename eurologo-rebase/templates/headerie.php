<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=0.9">
		<title>Eurologo.fr</title>
		<link rel="stylesheet" href="../css/materialize.min.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/materialize.min.js"></script>
		<link rel="stylesheet" href="../css/unslider.css">
		<link rel="stylesheet" href="../css/unslider-dots.css">
		<link rel="stylesheet" href="../css/style.css">
	</head>
	<body>
		<div class="container">
				<nav>
				<div class="nav-wrapper">
					<a href="index.php" class="brand-logo">
						<div class="eurolog">
							<img src="../wall/logoeurologo.png">
						</div>
					</a>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons"></i></a>
					<ul class="right hide-on-med-and-down">
						<li>
							<a href="index.php"><img src="../images/butterfly-icon.png"></a>
							Accueil
						</li>
						<li>
							<a href="panneaux.php"><img src="../images/butterflycyan.png"></a>
							Panneaux
						</li>
						<li>
							<a href="textile.php"><img src="../images/butterflyviolet.png"></a>
							Textile
						</li>
						<li>
							<a href="vehicules.php"><img src="../images/butterflyorange.png"></a>
							Véhicules
						</li>
						<li>
							<a href="stickers.php"><img src="../images/butterflygreen.png"></a>
							Stickers
						</li>
						<li>
							<a href="divers.php"><img src="../images/butterflytest.png"></a>
							Divers
						</li>
						<li>
							<a href="contact.php"><img src="../images/butterflyred.png"></a>
							Contact
						</li>
					</ul>
				</div>
			</nav>

