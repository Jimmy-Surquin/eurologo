<?php
include '../templates/headerie.php';
?>

<?php
if (ereg("MSIE", $_SERVER["HTTP_USER_AGENT"])) {
echo '<link href="../css/not.css" media="all" rel="stylesheet" type="text/css" />';
}?>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>

<hr>
<form id="contact" method="post" action="traitement.php">
	<fieldset>
		<legend>
			Vos coordonnées :
		</legend>
		<p>
			<label for="nom">Nom et prénom :</label>
			<input type="text" id="nom" name="nom" tabindex="1" />
		</p>
		<p>
			<label for="email">Email :</label>
			<input type="text" id="email" name="email" tabindex="2" />
		</p>
	</fieldset>
	<fieldset>
		<legend>
			Votre message (ou votre type de demande) :
		</legend>
		<p>
			<label for="objet">Objet :</label>
			<input type="text" id="objet" name="objet" tabindex="3" />
		</p>
		<p>
			<label for="message">Message :</label>
			<textarea id="message" name="message" tabindex="4" cols="30" rows="8"></textarea>
		</p>
	</fieldset>
	<div style="text-align:center;">
		<input type="submit" name="envoi" value="Envoyez votre message" />
	</div>
</form>

<?php
include '../templates/footerie.php';
?>

