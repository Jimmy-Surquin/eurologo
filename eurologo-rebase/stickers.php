<?php
include './templates/header.php';
?>
<center>
	<h1>Stickers</h1>
	<h3>De toutes formes,de toutes dimensions le sticker reste un bon moyen de véhiculer vos messages.</h3>
	<h5>
		Pour toute demande de prix merci de nous préciser la quantité, les dimensions et de nous joindre un fichier du visuel a imprimé en nous précisant si le sticker aurait une forme spécifique autre que rectangulaire.
	</h5>
</center>
<center>
	<div class="my-slider" class="responsive-img">
		<ul>
			<li>
				<img src="./photos/stickers/DSC01928.JPG">
				<center>
					<h3> Stickers #1</h3>
				</center>
			</li>
			<li>
				<img src="./photos/stickers/DSC01962.JPG">
				<center>
					<h3>Stickers #2</h3>
				</center>
			</li>
		</ul>
	</div>
</center>
<?php
include './js/scriptjs.js';
include './js/sidenav.js';
include './templates/footer.php';
?>
