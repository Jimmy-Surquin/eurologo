<?php
include './templates/header.php';
?>
<center>
	<h1>Divers</h1>
	<h3>Nos différents moyens techniques et la compétence de nos professionnels nous permettent de faire de réalisations diverses et variées.</h3>
</center>
<center>
	<div class="my-slider" class="responsive-img">
		<ul>
			<li>
				<img src="./photos/divers/DSC01570.JPG">
				<h3> Table de Management </h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01561.JPG">
				<h3> Totem (3 Artisans)</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Ballons</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Ameublement</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Sais ap</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Mug</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Facade</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Décoration meuble : Peugeot Box</h3>
			</li>
			<li>
				<img src="./photos/divers/DSC01743.JPG">
				<h3> Cabinet Médical</h3>
			</li>
		</ul>
	</div>
</center>
<?php
include './js/scriptjs.js';
include './js/sidenav.js';
include './templates/footer.php';
?>

