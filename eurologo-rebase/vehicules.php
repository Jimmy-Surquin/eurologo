<?php
include './templates/header.php';
?>
<center>
	<h1>Véhicules</h1>
	<span id="car"><h3>Le meilleur support publicitaire mobile : vos véhicules.Affiches adhésives,magnétiques,lettrage adhésif,photonumérique,semi-covering.Autant de supports pour afficher votre identité visuelle.</h3></span>
	<h5>
		Pour toute demande de prix merci de nous préciser la marque, le type et la couleur du véhicule et de nous joindre un fichier du visuel a imprimé ainsi que les informations que vous souhaitez afficher.
	</h5>
</center>
<center>
	<div class="my-slider" class="responsive-img">
		<ul>
			<li>
				<img src="./photos/camions/vehicule.JPG">
				<center>
					<h3>Véhicules 1 </h3>
				</center>
			</li>
			<li>
				<img src="./photos/camions/DSC02006.JPG">
				<center>
					<h3>Véhicules 2</h3>
				</center>
			</li>
			<li>
				<img src="./photos/camions/DSC02016.JPG">
				<center>
					<h3>Véhicules 3</h3>
				</center>
			</li>
			<li>
				<img src="./photos/camions/DSC01518.JPG">
				<center>
					<h3>Véhicules 3</h3>
				</center>
			</li>
		</ul>
	</div>
</center>
<?php
include './js/scriptjs.js';
include './js/sidenav.js';
include './templates/footer.php';
?>
