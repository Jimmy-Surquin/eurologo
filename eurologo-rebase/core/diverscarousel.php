<div class="carousel carousel-slider center" data-indicators="true">
    <div class="carousel-item black black-text" href="#one!">
      <h2>First Panel</h2>
      <p class="white-text">This is your first panel</p>
    </div>
    <div class="carousel-item black black-text" href="#two!">
<div id="test">
      <h2>Second Panel</h2>
      <p class="black-text">This is your second panel</p>
</div>
    </div>
    <div class="carousel-item black black-text" href="#three!">
      <h2>Third Panel</h2>
      <p class="white-text">This is your third panel</p>
    </div>
    <div class="carousel-item black black-text" href="#four!">
      <h2>Fourth Panel</h2>
      <p class="white-text">This is your fourth panel</p>
    </div>
  </div>
