<?php
include './templates/header.php';
?>
<center>
	<h1>Panneaux</h1>
	<h3>Nous vous proposons une large gamme de panneaux publicitaires de toutes dimensions, de tous matériaux, qui permettront de mettre en avant vos sociétés , vos réalisations, vos partenaires...</h3>
	<h5>
		Pour toute demande de prix merci de nous préciser le support, la quantité, les dimensions et de nous joindre un fichier du visuel a imprimé.
	</h5>
</center>
<center>
	<div class="my-slider" class="responsive-img">
		<ul>
			<li>
				<img src="./photos/panneaux/DSC01949.JPG">
				<center>
					<h3> Panneaux drapeaux Aquilux</h3>
				</center>
			</li>
			<li>
				<img src="./photos/panneaux/DSC01950.JPG">
				<center>
					<h3>Panneaux plat Aquilux</h3>
				</center>
			</li>
			<li>
				<img src="./photos/panneaux/DSC01542.JPG">
				<center>
					<h3>Panneaux publicitaire en dibond</h3>
				</center>
			</li>
			<li>
				<img src="./photos/panneaux/DSC01555.JPG">
				<center>
					<h3>Panneaux enseigne en dibond</h3>
				</center>
			</li>
		</ul>
	</div>
</center>
<?php
include './js/scriptjs.js';
include './js/sidenav.js';
include './templates/footer.php';
?>

